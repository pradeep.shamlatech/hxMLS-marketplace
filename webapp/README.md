# Marketplace Frontend

Decentraland's Marketplace

## Setup

0. Fill environment variables

```
$ mv .env.example .env
$ vi .env
```

1. Install dependencies

```
$ npm install
```

2. Run development server

```
$ npm start
```

3. Build for production

```
$ npm run build
```

## Generate TypeScript interfaces for contracts

If you want to regenerate the contract typings in `webapp/src/contract` do the following:

```
npx web3x-codegen
```

Important points:

``````
After npm install just check the status on cmd by
git status

Then 

You will get two changes of node_modules file related to Decentraland-dapps and Decentraland-ui

And only need to checkout that two files.


For confirmation that everyting is fine
check the root nodeModulechanges files in decenterland-dapps and decenterland-ui
for Nabar.js in decenterland-dapps and path is /node_modules/decentraland-dapps/dist/containers/Navbar/Navbar.js
for Navbar.tsx in decenterland-ui and path is /node_modules/decentraland-ui/src/components/Navbar/Navbar.tsx


in case of forget to checkout just copy the files from root and paste in nodemodules of webapp.
``````



import React from 'react'

import { Props } from './Menu.types'
import './Menu.css'

const Menu = (props: Props) => {
  const { className = '', children } = props
  // console.log("the classnames are : ", className,);
  // console.log("the classnames are : ", children);

  return <ul className={`Menu ${className}`}>{children}</ul>
}

export default React.memo(Menu)

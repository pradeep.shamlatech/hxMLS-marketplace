"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const decentraland_ui_1 = require("decentraland-ui");
const utils_1 = require("../../modules/translation/utils");
class Navbar extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.getTranslations = () => {
            if (!this.props.hasTranslations) {
                return undefined;
            }
            return {
                menu: {
                    avatars: React.createElement(utils_1.T, { id: "@dapps.navbar.menu.avatars" }),
                    marketplace: React.createElement(utils_1.T, { id: "@dapps.navbar.menu.marketplace" }),
                    // events: React.createElement(utils_1.T, { id: "@dapps.navbar.menu.events" }),
                    // agora: React.createElement(utils_1.T, { id: "@dapps.navbar.menu.agora" }),
                    // dao: React.createElement(utils_1.T, { id: "@dapps.navbar.menu.dao" }),
                    // docs: React.createElement(utils_1.T, { id: "@dapps.navbar.menu.docs" }),
                    // blog: React.createElement(utils_1.T, { id: "@dapps.navbar.menu.blog" }),
                    // builder: React.createElement(utils_1.T, { id: "@dapps.navbar.menu.builder" })
                },
                account: {
                    connecting: React.createElement(utils_1.T, { id: "@dapps.navbar.account.connecting" }),
                    signIn: React.createElement(utils_1.T, { id: "@dapps.navbar.account.signIn" })
                }
            };
        };
    }
    render() {
        return React.createElement(decentraland_ui_1.Navbar, Object.assign({}, this.props, { i18n: this.getTranslations() }));
    }
}
exports.default = Navbar;
//# sourceMappingURL=Navbar.js.map
